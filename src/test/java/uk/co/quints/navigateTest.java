package uk.co.quints;

import org.junit.Before;
import org.junit.Test;
import uk.co.quints.RoverNavigation.*;

import static org.junit.Assert.assertTrue;

public class navigateTest {

    Navigate navigate;
    Grid grid;
    @Before
    public void setup(){
        grid =new Grid(5,5);
        grid.add(new Rover(new Position(0,0), Direction.N));
        navigate=new Navigate(grid);
    }

    @Test
    public void moveOneForwardCorrectly(){
        char[] commands = {'f'};
        navigate.move(commands);
        assertTrue(new Position(0,1).equals(grid.getRover().getPosition()));
    }

    @Test
    public void moveFiveForwardCorrectly(){
        char[] commands = {'f','f','f','f','f'};
        navigate.move(commands);
        assertTrue(new Position(0,0).equals(grid.getRover().getPosition()));
    }
    @Test
    public void moveOneBackwardCorrectly(){
        char[] commands = {'b'};
        navigate.move(commands);
        assertTrue(new Position(0,4).equals(grid.getRover().getPosition()));
    }

    @Test
    public void moveTwoBackwardCorrectly(){
        char[] commands = {'b','b'};
        navigate.move(commands);
        assertTrue(new Position(0,3).equals(grid.getRover().getPosition()));
    }
    @Test
    public void moveFiveBackwardCorrectly(){
        char[] commands = {'b','b','b','b','b'};
        navigate.move(commands);
        assertTrue(new Position(0,0).equals(grid.getRover().getPosition()));
    }
    @Test
    public void moveEastForward(){
        char[] commands = {'r','f'};
        navigate.move(commands);
        assertTrue(new Position(1,0).equals(grid.getRover().getPosition()));
    }
    @Test
    public void moveSouthForward(){
        char[] commands = {'r','r','f'};
        navigate.move(commands);
        assertTrue(new Position(0,4).equals(grid.getRover().getPosition()));
    }
    @Test
    public void makeInvalidMoveThrowsException(){
        char[] commands = {'r','z','f'};
        boolean pass=false;
        try {
            navigate.move(commands);
            new Position(0, 4).equals(grid.getRover().getPosition());
        }catch (IllegalArgumentException exception){
            pass=true;
        }
        assertTrue(pass);
    }

    @Test
    public void moveWestForward(){
        char[] commands = {'l','l','l','l','l','f'};
        navigate.move(commands);
        assertTrue(new Position(4,0).equals(grid.getRover().getPosition()));
    }

    @Test
    public void movebackEast(){
        char[] commands = {'l','b'};
        navigate.move(commands);
        assertTrue(new Position(1,0).equals(grid.getRover().getPosition()));
    }

    @Test
    public void movebackWest(){
        char[] commands = {'r','b'};
        navigate.move(commands);
        assertTrue(new Position(4,0).equals(grid.getRover().getPosition()));
    }
}
