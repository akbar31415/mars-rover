package uk.co.quints;

import org.junit.Test;
import uk.co.quints.RoverNavigation.Direction;

import static junit.framework.Assert.assertEquals;

public class directionTest {

    //test Direction contain all four enum
    @Test
    public void directionContainsNorthEnum(){
        assertEquals("N", Direction.N.name());
    }
    @Test
    public void directionContainsSouthEnum(){
        assertEquals("S", Direction.S.name());
    }
    @Test
    public void directionContainsEastEnum(){
        assertEquals("E", Direction.E.name());
    }
    @Test
    public void directionContainsWestEnum(){
        assertEquals("W", Direction.W.name());
    }

}
