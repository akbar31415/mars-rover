package uk.co.quints;

import org.junit.Before;
import org.junit.Test;
import uk.co.quints.RoverNavigation.*;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class obstacleTest {
    Grid grid;

    @Before
    public void setup(){
        grid=new Grid(5,5);
    }

    @Test
    public void addOneObstacleOnTheGrid(){
        grid.addObstacle(0,2);
        assertEquals(1,grid.getNumberOfObstacles());
    }

    @Test
    public void addTwoObstacleOnTheGrid(){
        grid.addObstacle(0,2);
        grid.addObstacle(0,3);
        assertEquals(2,grid.getNumberOfObstacles());
    }


    @Test
    public void addObstacleOnTheEdgeOfTheNormalisedGridDoesNotThrowsException(){
        boolean pass=false;
        try{
            grid.addObstacle(4,4);
        }catch (IndexOutOfBoundsException exception){
            pass=true;
        }
        assertFalse(pass);
    }

    @Test
    public void obstacleOnSamePositionOnlyCountedOnce(){
        grid.addObstacle(0,2);
        grid.addObstacle(0,2);
        assertEquals(1,grid.getNumberOfObstacles());
    }

    @Test
    public void testAddedObstaclesReturnAsBloacked(){
        grid.addObstacle(0,2);
        assertTrue(grid.isBlocked(0,2));
        assertFalse(grid.isBlocked(0,1));
    }

    @Test
    public void testObstacleStopsRoverNavigation(){
        //arrange
        char[] commands = {'f','f','f','f','f'};
        grid.add(new Rover(0,0, Direction.N));
        grid.addObstacle(0,2);
        //act
        new Navigate(grid).move(commands);
        //assert
        assertTrue(new Position(0,1).equals(grid.getRover().getPosition()));
    }

    @Test
    public void testObstacleStopsRoverNavigation2(){
        //arrange
        char[] commands = {'f','f','f','f','f'};
        grid.add(new Rover(0,1,Direction.N));
        grid.addObstacle(0,0);
        //act
        new Navigate(grid).move(commands);
        //assert
        assertTrue(new Position(0,4).equals(grid.getRover().getPosition()));
    }

    @Test
    public void arrayGridPutObstacleInTheRightPlace(){
        int [][] gridValue={{0,0,0}, {0,0,0}, {1,0,0}, {0,0,0}};// (4 by 3 grid)
        grid= new Grid(gridValue);
        assertTrue(grid.isBlocked(0,2));
    }

    @Test
    public void initialiseGridWithObstacleInArrayBlockRoverNavigation(){
        //initialise grid with obstacle
        int [][] gridValue={{0,0,0},{0,0,0},{1,0,0},{0,0,0}};// (4 by 3 grid)
        char[] commands = {'f','f','f','f','f'};
        grid= new Grid(gridValue);
        grid.add(new Rover(0,0,Direction.N));
        //act
        new Navigate(grid).move(commands);
        //assert
        assertTrue(new Position(0,1).equals(grid.getRover().getPosition()));
    }

    @Test
    public void skipsAddingRoverIfPositionOccupiedByObstacle(){
        grid.addObstacle(0,0);
        grid.add(new Rover(0,0,Direction.N));
        assertTrue(grid.isEmpty());
    }

    @Test
    public void movellbb(){
        //arrange
        char[] commands = {'l','l','b','b'};
        grid.add(new Rover(0,0, Direction.N));
        grid.addObstacle(0,2);
        //act
        new Navigate(grid).move(commands);
        //assert
        assertTrue(new Position(0,1).equals(grid.getRover().getPosition()));
    }

    @Test
    public void addObstacleInvalidPositionCatchesException(){
        grid.addObstacle(0,5);
        assertEquals(grid.getNumberOfObstacles(),0);
    }
}
