package uk.co.quints;
import org.junit.Before;
import org.junit.Test;
import uk.co.quints.RoverNavigation.Direction;
import uk.co.quints.RoverNavigation.Grid;
import uk.co.quints.RoverNavigation.Position;
import uk.co.quints.RoverNavigation.Rover;

import static org.junit.Assert.*;

public class gridTest {

//    You are given the initial starting point (x,y) of a rover and the direction (N,S,E,W) it is facing.
//    The rover receives a character array of commands.
//    Implement commands that move the rover forward/backward (f,b).
//    Implement commands that turn the rover left/right (l,r).
//    Implement wrapping from one edge of the grid to another. (planets are spheres after all)
//    Implement obstacle detection before each move to a new square. If a given sequence of commands encounters an obstacle, the rover moves up to the last possible point and reports the obstacle.

    Grid grid;
    Rover rover;

    @Before
    public void setup(){
        grid =new Grid(5,5);
        rover=new Rover(1,1, Direction.N);
    }

    @Test
    public void addGridIsEmpty(){
        assertTrue(grid.isEmpty());
    }

    @Test
    public void addRoverToGrid(){
        grid.add(rover);
        assertFalse(grid.isEmpty());
    }

    @Test
    public void addOneRoverToGridOnlyAllowsSingleRover(){
        grid.add(rover);
        assertEquals(grid.numberOfRover(),1);
    }

    @Test
    public void addTwoRoverToGridOnlyAllowsSingleRover(){
        grid.add(rover);
        grid.add(rover);
        assertEquals(grid.numberOfRover(),1);
    }

    @Test
    public void roverPositionIsConsistent(){
        Position position=new Position(1,1);
        grid.add(rover);
        assertEquals(position,grid.getRover().getPosition());
    }

    @Test
    public void roverPositionIsCorrectlyNormalisedForLoopedGrid(){
        Position positionSet=new Position(5,5);
        Position positionExpected=new Position(0,0);
        grid.add(new Rover(positionSet,Direction.N));
        assertTrue(grid.getRover().getPosition().equals(positionExpected));
    }

    @Test
    public void roverPositionIsCorrectlyNormalisedForLoopedArrayedGrid(){
        //arrange
        int [][] gridValue={{0,0,0},{0,0,0},{0,0,0},{0,0,0}};
        grid=new Grid(gridValue);
        Position positionSet=new Position(0,3);
        Position positionExpected=new Position(0,0);
        //act
        grid.add(new Rover(positionSet,Direction.N));
        //assert
        assertTrue(grid.getRover().getPosition().equals(positionExpected));
    }


}
