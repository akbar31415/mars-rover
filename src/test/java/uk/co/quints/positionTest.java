package uk.co.quints;

import org.junit.Test;
import uk.co.quints.RoverNavigation.Position;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class positionTest {

    @Test
    public void testEqualOfSamePositionReturnTrue(){
        assertTrue(new Position(2,2).equals(new Position(2,2)));
    }
    @Test
    public void testEqualOfDifferentPositionReturnFalse(){
        assertFalse(new Position(2,2).equals(new Position(1,1)));
    }
}
