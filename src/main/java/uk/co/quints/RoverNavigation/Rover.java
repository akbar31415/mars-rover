package uk.co.quints.RoverNavigation;

public class Rover {

    private Position position;
    private Direction direction;

    public Rover(int xPosition, int yPosition, Direction direction) {
        setPosition(new Position(xPosition,yPosition));
        this.direction = direction;
    }

    public Rover(Position position, Direction direction) {
        setPosition(position);
        this.direction = direction;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }
}
