package uk.co.quints.RoverNavigation;

public class Grid {

    private int xLimit;
    private int yLimit;
    private int count=0;
    private Rover rover;
    private int[][] grid;

    public Grid(int xSize, int ySize) {
        this.xLimit = xSize -1;
        this.yLimit = ySize -1;
        this.grid= new int[ySize][xSize];
    }
    

    public Grid(int[][] gridValue) {
        xLimit=(gridValue.length)-1;
        yLimit=(gridValue[0].length)-1;
        this.grid=gridValue;
    }

    public boolean isEmpty() {
        return numberOfRover()==0;
    }

    public void add(Rover rover) {
        if(invalidRoverPosition(normalisePosition(rover))) {
            System.out.println("Cannot add Rover, the position is occupied by obstacle");
        }else if(isEmpty()) {
            count++;
            this.rover=rover;
            normalisePosition(rover);
        }else{
            System.out.println("A rover has already been added");
        }
    }

    public int numberOfRover() {
        return count;
    }

    public Rover normalisePosition(Rover rover){
        int x=rover.getPosition().getX();
        int y=rover.getPosition().getY();
        if(x> xLimit){
            rover.getPosition().setX(x% xLimit -1);
        }else if(x<0){
            rover.getPosition().setX(xLimit +x% xLimit +1);
        }
        if(y> yLimit){
            rover.getPosition().setY(y% yLimit -1);
        }else if(y<0){
            rover.getPosition().setY(yLimit +y% yLimit +1);
        }
        return rover;
    }

    public Rover getRover() {
        return rover;
    }

    public void addObstacle(int x, int y) {
        try {
            grid[y][x] = 1;
        }catch (ArrayIndexOutOfBoundsException exception){
            System.out.println("Not a valid normalised position, maximum position integer is one less than grid size");
        }
    }

    public int getNumberOfObstacles() {
        int countObstacles=0;
        for (int i = 0; i < xLimit+1; i++) {
            for (int j = 0; j < yLimit+1; j++) {
                if(grid[i][j]==1){
                    countObstacles++;
                }
            }
        }
        return countObstacles;
    }

    public boolean isBlocked(int x, int y) {
        return grid[y][x] == 1;
    }

    public boolean invalidRoverPosition(Rover rover){
        return isBlocked(rover.getPosition().getX(),rover.getPosition().getY());
    }
}
