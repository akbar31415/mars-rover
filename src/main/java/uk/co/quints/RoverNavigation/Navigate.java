package uk.co.quints.RoverNavigation;
import static uk.co.quints.RoverNavigation.Direction.*;

public class Navigate {


    private Grid grid;

    public Navigate(Grid grid) {
        this.grid=grid;
    }

    public String move(char[] commands) {
        for (char command:commands) {
            switch (command) {
                case 'f':
                    moveForward(grid.getRover());
                    if(grid.invalidRoverPosition(grid.getRover())){
                        moveBackward(grid.getRover());
                        return "Obstacle detected at position ("+grid.getRover().getPosition().getX()+","+grid.getRover().getPosition().getY()+")";
                    }
                    break;
                case 'b':
                    moveBackward(grid.getRover());
                    if(grid.invalidRoverPosition(grid.getRover())){
                        moveForward(grid.getRover());
                        return "Obstacle detected at position ("+grid.getRover().getPosition().getX()+","+grid.getRover().getPosition().getY()+")";
                    }
                    break;
                case 'l':
                    rotateAntiClockWise(grid.getRover());
                    break;
                case 'r':
                    rotateClockWise(grid.getRover());
                    break;
                default:
                    throw new IllegalArgumentException(command + " is not a valid move");
            }
        }
        return "No Obstacle detected";
    }


    private void rotateAntiClockWise(Rover rover) {
        set(rover, W, N, E, S);
    }

    private void rotateClockWise(Rover rover) {
        set(rover, E, S, W, N);
    }

    private void set(Rover rover, Direction e, Direction s, Direction w, Direction n) {
        switch (rover.getDirection()){
            case N: rover.setDirection(e);
                break;
            case E: rover.setDirection(s);
                break;
            case S: rover.setDirection(w);
                break;
            case W: rover.setDirection(n);
                break;
        }
    }

    private void moveForward(Rover rover) {
        switch (rover.getDirection()){
            case N: moveUp(rover);
                    break;
            case E: moveRight(rover);
                    break;
            case S: moveDown(rover);
                    break;
            case W: moveLeft(rover);
                    break;
        }
        grid.normalisePosition(rover);
    }

    private void moveBackward(Rover rover){
        switch (rover.getDirection()){
            case N: moveDown(rover);
                    break;
            case E: moveLeft(rover);
                    break;
            case S: moveUp(rover);
                    break;
            case W: moveRight(rover);
        }
        grid.normalisePosition(rover);
    }

    private void moveUp(Rover rover){
        rover.getPosition().setY(rover.getPosition().getY()+1);
    }
    private void moveDown(Rover rover){
        rover.getPosition().setY(rover.getPosition().getY()-1);
    }
    private void moveRight(Rover rover){
        rover.getPosition().setX(rover.getPosition().getX()+1);
    }
    private void moveLeft(Rover rover){
        rover.getPosition().setX(rover.getPosition().getX()-1);
    }
}
