package uk.co.quints.Main;
import uk.co.quints.RoverNavigation.*;
import java.util.Scanner;

public class Main {
    public static void main(String... args) {
        Scanner scanner = new Scanner(System.in);
        boolean run = true;
        Grid grid;
        while (run) {
            boolean addObstacleMode=true;
            System.out.println("What's the grid size? \"x y\" (Note that the position ranges from 0 to gridSize-1)");
            int x = scanner.nextInt();
            int y = scanner.nextInt();
            grid = new Grid(x, y);
            while (addObstacleMode) {
                scanner.nextLine();
                System.out.println("Add obstacle (y/n)?");
                String line=scanner.nextLine();
                if(line.equals("n")){
                    addObstacleMode=false;
                }else if(line.equals("y")){
                    System.out.println("What's the position of the obstacle "+(grid.getNumberOfObstacles()+1)+"?");
                    x = scanner.nextInt();
                    y = scanner.nextInt();
                    grid.addObstacle(x,y);
                }
            }
            while (grid.numberOfRover()!=1) {
                System.out.println("Type in Rover position and orientation \"x y N\"");
                x = scanner.nextInt();
                y = scanner.nextInt();

                try {
                    Direction direction =Direction.valueOf(scanner.next().toUpperCase());
                    grid.add(new Rover(new Position(x,y), direction));
                }catch (IllegalArgumentException e){
                    System.out.println("Please enter a valid orientation for the rover");
                }
                scanner.nextLine();
            }
            System.out.println("Type in path using l,r,b,f such as \"ffff\" will go forward 4 times");
            String path=scanner.nextLine();
            char[] charArray = path.toCharArray();
            String message=new Navigate(grid).move(charArray);
            System.out.println(message);
            System.out.println("Final rover position is ("+grid.getRover().getPosition().getX()+","+grid.getRover().getPosition().getY()+")");
            System.out.println("Type \"exit\" to finish or enter to restart");
            String line= scanner.nextLine();
            if(line.equals("exit")){
                run=false;
            }
        }
    }
}